import sympy
import base64
import sys , os




# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')

# Restore
def enablePrint():
    sys.stdout = sys.__stdout__

def sauvegarder_cle_publique(n, e, nom_fichier):
    with open(nom_fichier, "w") as f:
        f.write('---begin monRSA public key---\n')
        
        # Convertir n et e en hexadécimal, les séparer par un retour chariot, et encoder en base64
        cle = f"{hex(n)}\n{hex(e)}"
        cle_base64 = base64.b64encode(cle.encode('ascii')).decode('ascii')
        f.write(cle_base64 + '\n')
        
        f.write('---end monRSA key ---\n')

def sauvegarder_cle_privee(n, d, nom_fichier):
    with open(nom_fichier, "w") as f:
        f.write('---begin monRSA private key---\n')
        
        # Convertir n et d en hexadécimal, les séparer par un retour chariot, et encoder en base64
        cle = f"{hex(n)}\n{hex(d)}"
        cle_base64 = base64.b64encode(cle.encode('ascii')).decode('ascii')
        f.write(cle_base64 + '\n')
        
        f.write('---end monRSA key---\n')

def keygen(nomFichier=""):
    #p et q deux nombres premiers distincts
    p=sympy.randprime(1000000000, 9999999999)
    q=sympy.randprime(1000000000, 9999999999)
    #n le produit de p et q
    n=p*q
    nprime=(p-1)*(q-1)

    #e est premier et différent de d
    #ed = 1 mod nprime
    e=sympy.randprime(1, nprime)
    d=pow(e, -1, nprime)
    if(nomFichier!=""):
        sauvegarder_cle_publique(n, e, nomFichier+'.pub')
        sauvegarder_cle_privee(n, d, nomFichier+'.priv')
        print("clés sauvegardés dans ",nomFichier,".priv et ",nomFichier,".pub")
    else:
        sauvegarder_cle_publique(n, e, 'monRSA.pub')
        sauvegarder_cle_privee(n, d, 'monRSA.priv')
        print("clés sauvegardés dans monRSA.priv et monRSA.pub")
    return n, e, d


#rsa encrypt
def encrypt(e,n,b):
    c=pow(b,e,n)
    return c
#rsa decrypt
def decrypt(c,d,n):
    b=pow(c,d,n)
    return b


#encode string in ascii list, each ascii code is 3 digits
def string_to_ascii_list(string):
    tab=['0']*len(string)
    for i in range(len(string)):
        tab[i]=str(ord(string[i]))
        while len(tab[i])<3:
            tab[i]='0'+tab[i]
    return tab
#encode int list to string
def int_list_to_string(int_list):
    return ''.join([str(char) for char in int_list])

#cut string in blocks of n-1 length from the end
def cut_string_in_blocks_from_end(string, n):
    cutted_string = []
    for i in range(0, len(string), n):
        cutted_string.insert(0,string[-n:])
        string=string[:-n]
    #fill first block with 0 before
    if len(cutted_string[0]) != n:
        for i in range(n-len(cutted_string[0])):
            cutted_string[0] = '0'+cutted_string[0]
    return cutted_string

#cut string in blocks of n-1 length from the end
def cut_string_in_blocks_from_end_no_zero(string, n):
    cutted_string = []
    for i in range(0, len(string), n):
        cutted_string.insert(0,string[-n:])
        string=string[:-n]
    return cutted_string

#cut number in list of int<128 by using pop           
def cut_number_in_list_V2(number):
    number=list(number)
    nc=[]
    l=[]
    while number!=[]:
        nc.append(number.pop(0))
        if number==[]:
            nc=int(''.join([str(char) for char in nc]))
            l.append(int(nc))
            break
        else:
            nc.append(number.pop(0))
            nc=int(''.join([str(char) for char in nc]))
            
        nc=str(nc)
        if number==[]:
            l.append(int(nc))
            break

        numtotest=int(nc+number[0])
        
        if numtotest<128 and number!=[]:
            l.append(numtotest)
            number.pop(0)
        else:
            l.append(int(nc)) 
        nc=[]
    return l

def decouper_nombre(n):
    chaine = str(n)
    liste_nombres = []
    nombre_courant = ""
    for chiffre in chaine:
        if chiffre == '0' and nombre_courant:
            liste_nombres.append(int(nombre_courant))
            nombre_courant = ""
        if chiffre == '0':
            liste_nombres.append(0)
            continue
        if int(nombre_courant + chiffre) < 128:
            nombre_courant += chiffre
        else:
            liste_nombres.append(int(nombre_courant))
            nombre_courant = chiffre
    if nombre_courant:
        liste_nombres.append(int(nombre_courant))
    
    return liste_nombres

def rsa_crypt(message,cle):
    if (flag_p!=1):
        blockPrint()
    n,e=lire_cle_publique(cle)
    longueur=len(str(n))
    #Transformer chaque caractère en entrée en un chiffre en utilisant le code ASCII
    ascii_list=string_to_ascii_list(message)
    ascii=''.join(ascii_list)
    print("string ascii:",ascii)
    #Assembler & redécouper cette chaîne en blocs
    print("longueur:",longueur)
    clist=cut_string_in_blocks_from_end(ascii,longueur-1)
    print("string découpé:" ,clist)
    #Chaque bloc en clair B est chiffré en un bloc C par la formule C = B^e mod n
    for i in range(len(clist)):
        clist[i]=encrypt(e,n,int(clist[i]))
    print("string chiffré assemblé:",clist)
    clist=add_zeros_to_blocks(clist,longueur)
    print("string chiffré avec zeros:",clist)
    #Assembler les blocs chiffrés C en une suite de chiffres
    c=int_list_to_string(clist)
    print("string chiffré:    ",c)
    #Cut en nombre < 128 pour conversion ascii
    clist=decouper_nombre(c)
    print("ascii number list:",clist)
    #Convertir chaque nombre en ASCII
    clist=[chr(char) for char in clist]
    print("ascii char list:",clist)
    #Encoder chaque char en base 64
    c=''.join(clist)
    c=base64.b64encode(c.encode('ascii'))
    enablePrint()
    print("message chiffré :",c)
    return c

#fonction qui ajoute des 0 a gauche des block pour faire des blocks de longueur n
def add_zeros_to_blocks(clist, longueur):
    return [str(nombre).rjust(longueur, '0') for nombre in clist]

#fonction qui ajoute des 0 a gauche des block pour faire des blocks de longueur n
def add_zeros_to_blocks_but_not_first(clist, longueur):
    return [str(clist[0])] + [str(nombre).rjust(longueur, '0') for nombre in clist[1:]]

def rsa_decrypt(code,cle):
    if (flag_p!=1):
        blockPrint()
    n,d=lire_cle_privee(cle)
    #decode base64
    c=base64.b64decode(code)
    c=c.decode('ascii')
    print("ascii char :",c)
    #decode ascii
    clist=[ord(char) for char in c]
    print("ascii number list:",clist)
    #cut en block de n-1 de long
    c=''.join([str(char) for char in clist])
    print("chaine chiffré ascii :",c)
    longueur=len(str(n))
    clist=cut_string_in_blocks_from_end_no_zero(c,longueur)
    print("blocks chiffrés :",clist)
    #dechiffrer chaque bloc
    for i in range(len(clist)):
        clist[i]=decrypt(int(clist[i]),d,n)
    print("blocs déchiffrés :",clist)
    clist=add_zeros_to_blocks_but_not_first(clist,longueur-1)
    #assembler les blocs
    c=int_list_to_string(clist)
    print("blocs assemblès :",c)
    #découper en chaîne de nombres ascii
    clist=cut_number_in_list_V2(c)
    print("ascii num",clist)
    #convertir chaque nombre en caractère ASCII
    clist=[chr(char) for char in clist]
    print("ascii tab:",clist)
    #assembler les caractères en une chaîne
    c=''.join(clist)
    enablePrint()
    print("message dechiffré: ",c)
    return c

def lire_cle_publique(nom_fichier):
    with open(nom_fichier, "r") as f:
        # Vérifier la première ligne
        premiere_ligne = f.readline().strip()
        if premiere_ligne != '---begin monRSA public key---':
            raise ValueError("Le fichier ne semble pas être une clé publique monRSA valide.")
        
        # Lire et décoder la ligne Base64
        ligne_base64 = f.readline().strip()
        cle_hex = base64.b64decode(ligne_base64).decode('ascii')
        
        # Séparer et convertir n et e de l'hexadécimal au décimal
        n_hex, e_hex = cle_hex.split('\n')
        n = int(n_hex, 16)
        e = int(e_hex, 16)
        
    return n, e

def lire_cle_privee(nom_fichier):
    with open(nom_fichier, "r") as f:
        # Vérifier la première ligne
        premiere_ligne = f.readline().strip()
        if premiere_ligne != '---begin monRSA private key---':
            raise ValueError("Le fichier ne semble pas être une clé privée monRSA valide.")
        
        # Lire et décoder la ligne Base64
        ligne_base64 = f.readline().strip()
        cle_hex = base64.b64decode(ligne_base64).decode('ascii')
        
        # Séparer et convertir n et d de l'hexadécimal au décimal
        n_hex, d_hex = cle_hex.split('\n')
        n = int(n_hex, 16)
        d = int(d_hex, 16)
        
    return n, d


def afficher_aide():
    texte = """
    Script monRSA par Benoit
    ====================================

    Syntaxe :
    monRSA <commande> [<clé>] [<texte>] [switchs]

    Commandes :
    - keygen : Génére une paire de clé
    - crytp : Chiffre <texte> avec la clé publique <clé>
    - decrytp: Déchiffre <texte> avec la clé privée <clé>
    - help : Affiche ce manuel

    Clé :
    Un fichier qui contient une clé publique monRSA ("crypt") 
    ou une clé privée ("decrypt").

    Texte :
    Une phrase en clair ("crypt") ou une phrase chiffrée ("decrypt").

    Switchs:
    -f <file> : permet de choisir le nom des clés générées,
                monRSA.pub et monRSA.priv par défaut.
    -p :  permet d'activer les prints dans le terminal
    ====================================
    """

    print(texte)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Erreur: La commande est obligatoire.")
        afficher_aide()
        sys.exit(1)
    
    nomFichier = "monRSA"
    flag_p=0
    if '-f' in sys.argv:
        index_f = sys.argv.index('-f')
        if index_f + 1 < len(sys.argv):
            nomFichier = sys.argv[index_f + 1]
            del sys.argv[index_f:index_f+2]
        else:
            print("Erreur: L'option -f nécessite un nom de fichier comme argument.")
            sys.exit(1)
    if '-p' in sys.argv:
        index_p = sys.argv.index('-p')
        del sys.argv[index_p]
        flag_p=1
    commande = sys.argv[1]
    if commande == "keygen":
        keygen(nomFichier)
    elif commande == "crypt":
        if len(sys.argv) < 4:
            print("Erreur: Les paramètres clé et texte sont obligatoires pour 'crytp'.")
            afficher_aide()
            sys.exit(1)
        cle = sys.argv[2]
        texte = sys.argv[3]
        rsa_crypt(texte,cle)
    elif commande == "decrypt":
        if len(sys.argv) < 4:
            print("Erreur: Les paramètres clé et texte sont obligatoires pour 'decrypt'.")
            afficher_aide()
            sys.exit(1)
        cle = sys.argv[2]
        texte = sys.argv[3]
        rsa_decrypt(texte,cle)
    elif commande == "help":
        afficher_aide()
    else:
        print(f"Erreur: Commande inconnue '{commande}'.")
        afficher_aide()
        sys.exit(1)

