def decouper_nombre(n):
    # Convertir le nombre en chaîne de caractères
    chaine = str(n)
    
    # Initialiser la liste des nombres découpés
    liste_nombres = []
    
    # Initialiser la chaîne temporaire pour stocker les chiffres du nombre actuel
    nombre_courant = ""
    
    # Parcourir chaque chiffre de la chaîne
    for chiffre in chaine:
        # Si le chiffre courant est '0', et le nombre courant n'est pas vide,
        # ajouter le nombre courant à la liste et réinitialiser le nombre courant
        if chiffre == '0' and nombre_courant:
            liste_nombres.append(int(nombre_courant))
            nombre_courant = ""
        # Ajouter le chiffre '0' à la liste et continuer
        if chiffre == '0':
            liste_nombres.append(0)
            continue
        # Si ajouter ce chiffre au nombre courant le rend toujours inférieur à 128
        if int(nombre_courant + chiffre) < 128:
            # Ajouter le chiffre au nombre courant
            nombre_courant += chiffre
        else:
            # Sinon, ajouter le nombre courant à la liste et commencer un nouveau nombre
            liste_nombres.append(int(nombre_courant))
            nombre_courant = chiffre
    
    # Ajouter le dernier nombre à la liste s'il n'est pas vide
    if nombre_courant:
        liste_nombres.append(int(nombre_courant))
    
    return liste_nombres

# Tester la fonction
nombre = 1652004
print(decouper_nombre(nombre))  # Devrait afficher: [16, 5, 20, 0, 4]
